package io.scenarium.logger;

public enum LogLevel {
	NONE(-3), PRINT(-2), TODO(-1), CRITICAL(0), ERROR(1), WARNING(2), INFO(3), DEBUG(4), VERBOSE(5);

	public final int value;

	LogLevel(int value) {
		this.value = value;
	}

	public static LogLevel fromString(String logLevel) {
		String value = logLevel.toUpperCase();
		LogLevel[] values = LogLevel.values();
		for (LogLevel elem : values)
			if (value.contentEquals(elem.name()) || value.contentEquals(String.valueOf(elem.value)))
				return elem;
		return ERROR;
	}

	public boolean isLessEqual(LogLevel other) {
		return this.value <= other.value;
	}

}
