/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.logger;

import java.nio.CharBuffer;
import java.util.HashMap;
import java.util.List;

import io.scenarium.logger.internal.Log;

@SuppressWarnings("unused")
public class Logger {
	static class WrapInt {
		public int value;
		
		public WrapInt(final int val) {
			this.value = val;
		}
	}
	
	// background colors
	private static final String BASH_COLOR_BG_BLACK = "\033[40m";
	
	private static final String BASH_COLOR_BG_BLUE = "\033[44m";
	
	private static final String BASH_COLOR_BG_CYAN = "\033[46m";
	private static final String BASH_COLOR_BG_GREEN = "\033[42m";
	private static final String BASH_COLOR_BG_MAGENTA = "\033[45m";
	private static final String BASH_COLOR_BG_RED = "\033[41m";
	private static final String BASH_COLOR_BG_WHITE = "\033[47m";
	private static final String BASH_COLOR_BG_YELLOW = "\033[43m";
	// regular colors
	private static final String BASH_COLOR_BLACK = "\033[0;30m";
	private static final String BASH_COLOR_BLUE = "\033[0;34m";
	// emphasized (bolded) colors
	private static final String BASH_COLOR_BOLD_BLACK = "\033[1;30m";
	private static final String BASH_COLOR_BOLD_BLUE = "\033[1;34m";
	private static final String BASH_COLOR_BOLD_CYAN = "\033[1;36m";
	private static final String BASH_COLOR_BOLD_GREEN = "\033[1;32m";
	private static final String BASH_COLOR_BOLD_MAGENTA = "\033[1;35m";
	private static final String BASH_COLOR_BOLD_RED = "\033[1;31m";
	private static final String BASH_COLOR_BOLD_WHITE = "\033[1;37m";
	private static final String BASH_COLOR_BOLD_YELLOW = "\033[1;33m";
	private static final String BASH_COLOR_CYAN = "\033[0;36m";
	private static final String BASH_COLOR_GREEN = "\033[0;32m";
	private static final String BASH_COLOR_MAGENTA = "\033[0;35m";
	// Return to the normal color settings
	private static final String BASH_COLOR_NORMAL = "\033[0m";
	private static final String BASH_COLOR_RED = "\033[0;31m";
	private static final String BASH_COLOR_WHITE = "\033[0;37m";
	private static final String BASH_COLOR_YELLOW = "\033[0;33m";
	// go to the Top of bash
	private static final String BASH_GO_TOP = "\033[0;0f";
	static boolean criticalOccured = false;
	
	private static LogLevel defaultLevel = LogLevel.INFO;
	private static boolean haveClassName = false;
	// to enable color, you need to install in eclipse the plug-in "ANSI escape in console"
	private static boolean haveColor = true;
	private static boolean haveFileName = true;
	private static boolean haveLibName = true;
	private static boolean haveLineNumber = true;
	private static boolean haveThreadId = true;
	private static boolean haveThreadName = false;
	private static boolean isInit = false;
	private static HashMap<String, LogLevel> logLevels = new HashMap<>();
	private static WrapInt sizeClassName = new WrapInt(25);
	private static WrapInt sizeFileName = new WrapInt(15);
	private static WrapInt sizeLibName = new WrapInt(0);
	private static WrapInt sizeLineNumber = new WrapInt(3);
	private static WrapInt sizeThreadId = new WrapInt(2);
	private static WrapInt sizeThreadName = new WrapInt(10);
	static {
		// Load system color state
		String value = System.getProperty("logger.color");
		if (value != null) {
			if (value.contentEquals("true") || value.contentEquals("1")) {
				Logger.haveColor = true;
			} else if (value.contentEquals("false") || value.contentEquals("0")) {
				Logger.haveColor = false;
			} else {
				System.out.println("error in color state '" + value + "' ==> not in range [true, false, 0, 1]");
			}
		}
		
		// Load system debug level
		value = System.getProperty("logger.level");
		if (value != null) {
			final LogLevel level = LogLevel.fromString(value);
			System.out.println("Change global level at " + value + " ==> " + level);
			Logger.defaultLevel = level;
		}
	}
	
	private static String addGeneric(final String level, final String libName) {
		String out = level;
		out += Logger.printElement("", "", Logger.haveLibName, libName, Logger.sizeLibName);
		out += Logger.printElement(" | ", "", Logger.haveThreadId, Long.toString(Thread.currentThread().getId()), Logger.sizeThreadId);
		out += Logger.printElement(" | ", "", Logger.haveThreadName, Thread.currentThread().getName(), Logger.sizeThreadName);
		if (Logger.haveClassName || Logger.haveFileName || Logger.haveLineNumber) {
			final StackTraceElement callElem = Logger.getCallerElement();
			out += Logger.printElementLeft(" | l=", "", Logger.haveLineNumber, Long.toString(callElem.getLineNumber()), Logger.sizeLineNumber);
			out += Logger.printElement(" | ", "", Logger.haveClassName, callElem.getClassName(), Logger.sizeClassName);
			out += Logger.printElement(" | ", "", Logger.haveFileName, callElem.getFileName(), Logger.sizeFileName);
		}
		return out;
	}
	
	public static void critical(final String libName, final String data) {
		if (Logger.haveColor) {
			System.out.print(Logger.BASH_COLOR_BOLD_RED);
		}
		System.out.println(Logger.addGeneric("[C] ", libName) + " | " + data);
		if (Logger.haveColor) {
			System.out.print(Logger.BASH_COLOR_NORMAL);
		}
		System.out.flush();
		if (Logger.haveColor) {
			System.out.print(Logger.BASH_COLOR_YELLOW);
		}
		for (final StackTraceElement ste : Thread.currentThread().getStackTrace()) {
			System.out.println(ste);
		}
		System.out.print(Logger.BASH_COLOR_NORMAL);
		System.out.flush();
		Logger.criticalOccured = true;
		System.exit(-50);
	}
	
	public static void debug(final String libName, final String data) {
		if (Logger.haveColor) {
			System.out.print(Logger.BASH_COLOR_YELLOW);
		}
		System.out.println(Logger.addGeneric("[D] ", libName) + " | " + data);
		if (Logger.haveColor) {
			System.out.print(Logger.BASH_COLOR_NORMAL);
		}
	}
	
	public static void displayBackTrace(final String libNameDraw) {
		StackTraceElement[] list = Thread.currentThread().getStackTrace();
		Logger.error(libNameDraw, "Display Stacktrace :");
		for (int iii = 0; iii < list.length; iii++) {
			Logger.error(libNameDraw, "    - " + list[iii]);
		}
		
	}
	
	public static void error(final String libName, final String data) {
		if (Logger.haveColor) {
			System.out.print(Logger.BASH_COLOR_RED);
		}
		System.out.println(Logger.addGeneric("[E] ", libName) + " | " + data);
		if (Logger.haveColor) {
			System.out.print(Logger.BASH_COLOR_NORMAL);
		}
	}
	
	private static StackTraceElement getCallerElement() {
		StackTraceElement[] tmpp = Thread.currentThread().getStackTrace();
		if (tmpp.length>5) {
			return tmpp[5];
		}
		if (tmpp.length>4) {
			return tmpp[4];
		}
		if (tmpp.length>3) {
			return tmpp[3];
		}
		if (tmpp.length>2) {
			return tmpp[2];
		}
		if (tmpp.length>1) {
			return tmpp[1];
		}
		return tmpp[0];
	}
	
	/** This function permit to get the printable string to print in the log element (select here the number of char to print)
	 * @param libName Name of the library
	 * @return string to set in the logger information */
	public static String getDrawableName(final String libName) {
		return libName; // String.format("%1$" + 15 + "s", libName);
	}
	
	public static boolean getNeedPrint(final String libName, final LogLevel level) {
		final LogLevel reference = Logger.logLevels.get(libName);
		if (reference == null) {
			return level.isLessEqual(Logger.defaultLevel);
		}
		return level.isLessEqual(reference);
	}
	
	public static void info(final String libName, final String data) {
		final StackTraceElement callElem = Logger.getCallerElement();
		if (Logger.haveColor) {
			System.out.print(Logger.BASH_COLOR_CYAN);
		}
		System.out.println(Logger.addGeneric("[I] ", libName) + " | " + data);
		if (Logger.haveColor) {
			System.out.print(Logger.BASH_COLOR_NORMAL);
		}
	}
	
	/** Initialize the library with generic argument in command line
	 * @param args list of argument that are filtered after */
	public static void init(final List<String> args) {
		if (Logger.isInit) {
			return;
		}
		Logger.isInit = true;
		for (int iii = 0; iii < args.size(); ++iii) {
			final String data = args.get(iii);
			if (data.startsWith("--log-level=")) {
				final String value = data.substring(12);
				final LogLevel level = LogLevel.fromString(value);
				System.out.println("Change global level at " + value + " ==> " + level);
				Logger.defaultLevel = level;
			} else if (data.contentEquals("--log-color")) {
				Logger.haveColor = true;
			} else if (data.contentEquals("--log-no-color")) {
				Logger.haveColor = false;
			} else if (data.startsWith("--log-lib=")) {
				final String value = data.substring(10);
				String[] values = value.split("/");
				if (values.length != 2) {
					values = value.split(":");
					if (values.length != 2) {
						values = value.split("\\+");
						if (values.length != 2) {
							System.err.println("Can not set the --logger-lib= with value='" + value + "' not formated name:X or name/X or name+X");
							continue;
						}
					}
				}
				System.out.println("Change level of '" + values[0] + "' at " + LogLevel.fromString(values[1]));
				Logger.logLevels.put(values[0], LogLevel.fromString(values[1]));
			}
		}
		// Clear all logger elements.
		int iii = 0;
		while (iii < args.size()) {
			final String data = args.get(iii);
			if (data.startsWith("--log")) {
				args.remove(iii);
			} else {
				iii++;
			}
		}
	}
	
	public static boolean isCriticalOccured() {
		return Logger.criticalOccured;
	}
	
	public static void print(final String libName, final String data) {
		if (Logger.haveColor) {
			System.out.print(Logger.BASH_COLOR_WHITE);
		}
		System.out.println(Logger.addGeneric("[P] ", libName) + " | " + data);
		if (Logger.haveColor) {
			System.out.print(Logger.BASH_COLOR_NORMAL);
		}
		System.out.flush();
	}
	
	private static String printElement(final String separator, final String separatorEnd, final boolean isActive, String data, final WrapInt size) {
		if (!isActive) {
			return "";
		}
		if (data == null) {
			data = "???";
		}
		final int stringSize = data.length();
		if (size.value < stringSize) {
			size.value = stringSize;
		}
		final int rest = size.value - stringSize;
		return separator + data + Logger.spaces(rest) + separatorEnd;
	}
	
	private static String printElementLeft(final String separator, final String separatorEnd, final boolean isActive, String data, final WrapInt size) {
		if (!isActive) {
			return "";
		}
		if (data == null) {
			data = "???";
		}
		final int stringSize = data.length();
		if (size.value < stringSize) {
			size.value = stringSize;
		}
		final int rest = size.value - stringSize;
		return separator + Logger.spaces(rest) + data + separatorEnd;
	}
	
	public static String spaces(final int spaces) {
		return CharBuffer.allocate(spaces).toString().replace('\0', ' ');
	}
	
	public static void todo(final String libName, final String data) {
		if (Logger.haveColor) {
			System.out.print(Logger.BASH_COLOR_WHITE);
		}
		System.out.println(Logger.addGeneric("[TODO] ", libName) + " | " + data);
		if (Logger.haveColor) {
			System.out.print(Logger.BASH_COLOR_NORMAL);
		}
	}
	
	public static void usage() {
		Log.print("    [log]:        Log session interface");
		Log.print("        --log-level=             Change the default log level (set all Log level):");
		Log.print("            -3/none:                 debug None");
		Log.print("            -2/print:                debug Print");
		Log.print("            -1/todo:                 debug Todo");
		Log.print("            0/critical:              debug Critical (default)");
		Log.print("            1/error:                 debug Error");
		Log.print("            2/warning:               debug Warning");
		Log.print("            3/info:                  debug Info");
		Log.print("            4/debug:                 debug Debug");
		Log.print("            5/verbose:               debug Verbose");
		Log.print("        --log-lib=name:X         Set a library specific level:");
		Log.print("            name                     Name of the library");
		Log.print("            X                        Log level to set [0..6]");
		Log.print("            !note!:  ':' can be replace with '/' or '+'");
		Log.print("        --log-color              Enable color in log");
		Log.print("        --log-no-color           Disable color in log (default)");
		Log.print("");
	}
	
	public static void verbose(final String libName, final String data) {
		if (Logger.haveColor) {
			System.out.print(Logger.BASH_COLOR_WHITE);
		}
		System.out.println(Logger.addGeneric("[V] ", libName) + " | " + data);
		if (Logger.haveColor) {
			System.out.print(Logger.BASH_COLOR_NORMAL);
		}
	}
	
	public static void warning(final String libName, final String data) {
		if (Logger.haveColor) {
			System.out.print(Logger.BASH_COLOR_MAGENTA);
		}
		System.out.println(Logger.addGeneric("[W] ", libName) + " | " + data);
		if (Logger.haveColor) {
			System.out.print(Logger.BASH_COLOR_NORMAL);
		}
	}
	
	private Logger() {}
}
