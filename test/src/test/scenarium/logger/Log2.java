package test.scenarium.logger;

import io.scenarium.logger.LogLevel;
import io.scenarium.logger.Logger;

public class Log2 {
	private static final String LIB_NAME = "sc-log-test-2";
	private static final String LIB_NAME_DRAW = Logger.getDrawableName(LIB_NAME);
	private static final boolean PRINT_DEBUG = Logger.getNeedPrint(LIB_NAME, LogLevel.DEBUG);

	private Log2() {}

	public static void debug(String data) {
		if (PRINT_DEBUG)
			Logger.debug(LIB_NAME_DRAW, data);
	}

}
