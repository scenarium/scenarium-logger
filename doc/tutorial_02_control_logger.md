Control the log level
=====================

Change the Level of the Log:
============================

With the system property:

  - ```logger.level```: Change the level of the log display:
      - critical
      - error
      - warning
      - info
      - debug
      - verbose;
  - ```logger.color```: Change the color of the log (true/false)


The second solution depend of the appication capability (check --help)


Log in color information
========================

If the color is enable thing to change the default console of eclipse, to support the ascii color code.



